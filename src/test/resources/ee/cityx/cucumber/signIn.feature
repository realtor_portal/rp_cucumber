@signIn
Feature: Sign In via username and password

    Background:
        Given I open http://cityx.ee/portal

    Scenario: Sign In via correct username and password
        When I sign in
        Then I see logout
        And I logout

    Scenario: Check validation message with invalid user name
        When I sign invalid user name
        Then I should see the validation error message

    Scenario: Check validation message with invalid passwword
        When I sign invalid password
        Then I should see the validation error message

    Scenario: Check validation message with empty fields
        Then I sign in with empty fields
        And I should see the validation error message

    @content
    Scenario: I want to check that my list does't display after login by another user
        When I sign in
        And I logout
        When I login by another user
        And I logout




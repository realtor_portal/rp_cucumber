@signInAndCreateNewPropertyAndTestReferenceLink
Feature: Sign In via username and password after that I check if property reference link opens

    Background:
    Scenario: Sign In, create new property, Open application link and test if application is created
        Given I open http://cityx.ee/portal
        And I sign in

        Then I click on Properties
        And I click on Add new property testNimi testCity testStreet 11


        Then I click on View button and fill reference application 39008160000 testFirstName testLastName test@test.com 12345678 refferalText 300 ThantsWhyIveSelected
        And I check if reference is submitted successfully

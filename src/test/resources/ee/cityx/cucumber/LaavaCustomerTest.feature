# new feature
# Tags: optional

Feature: create customer data from specified CSV file

    Scenario: Sign In, create new customers for CSV data
        Given I open http://cityx.ee/portal
        And I sign in

        Then I click on menu dropdown and open costumer view
        Then I create Costumers according CSV file src\test\resources\csv\customer_laava.csv

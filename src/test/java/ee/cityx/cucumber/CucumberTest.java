package ee.cityx.cucumber;

import com.codeborne.selenide.Configuration;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
    tags = {"@signIn"},
    format = {"pretty", "json:target/cucumber.json", "html:target/cucumber.html"},
    features = {"src/test/resources/ee/cityx/cucumber"},
    glue = "ee.cityx.cucumber"
)
public class CucumberTest {

    @BeforeClass
    public static void setUp() {
        Configuration.browser = "chrome";
        Configuration.timeout = 1000;
        Configuration.fastSetValue = false;
        Configuration.baseUrl = "http://cityx.ee/portal";
    }
}

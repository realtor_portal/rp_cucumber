package ee.cityx.cucumber.steps;

import com.codeborne.selenide.Configuration;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import ee.cityx.cucumber.page.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.codeborne.selenide.Selenide.open;

public class StepDefinitions {


    @Given("^I open (.*)$")
    public void iOpenCityx(String url) {
        Configuration.browser = "chrome";
        open(url);
    }

    @Then("^I see logout$")
    public void iSeeLogout() {
        DashboardPage.logoutExists();
    }

    @Then("^I sign in$")
    public void sign_in() {
        LoginPage.signIn("user", "user");
    }

    @Then("^I sign in as admin$")
    public void sign_in_as_admin() {
        LoginPage.signIn("admin", "admin");
    }
    @Then("^I click on Properties$")
    public void click_on_property() {
        DashboardPage.clickOnProperties();
    }


    @Then("^I sign invalid user name$")
    public void i_sign_invalid_user_name() {
        LoginPage.signIn("user123", "user");
    }

    @Then("^I sign in with empty fields$")
    public void i_sign_in_with_empty_fields() {
        LoginPage.signIn("", "");
    }

    @Then("^I sign invalid password$")
    public void i_sign_invalid_password() {
        LoginPage.signIn("user", "user1");
    }

    @Then("^I should see the validation error message$")
    public void i_should_see_the_validation_error_messages() {
        LoginPage.errorMessagesVisible();
        LoginPage.closeLoginPopup();
    }

    @Then("^I logout$")
    public void logout() {
        DashboardPage.logout();
    }

    @Then("^I login by another user$")
    public void i_login_by_another_user() {
        LoginPage.signIn("admin", "admin");
    }


    @Then("^I click on Add new property (.*) (.*) (.*) (.*)$")
    public void i_click_new_proprty(String name, String city, String street, String streetNr) throws Throwable {
        PropertiesPage.createNewProperty(name,city,street,streetNr);
        //throw new PendingException();
    }

    @Then("^I click on View button and fill reference application (.*) (.*) (.*) (.*) (\\d+) (.*) (\\d+) (.*)$")
    public void iClickOnViewButtonAndFillReferenceApplication(String isikukood, String firstName, String lastName, String email, Integer phoneNumber, String referral, Integer rent, String question) {
        Date date = new Date();
        Date datePlusMonth = new Date();
        Calendar myCal = Calendar.getInstance();
        myCal.setTime(datePlusMonth);
        myCal.add(Calendar.MONTH, +2);
        datePlusMonth = myCal.getTime();

        String strDateFormat = "yyyy-MM-dd";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);

        String formattedMoveInDate= dateFormat.format(date);
        String formattedMoveOutDate= dateFormat.format(datePlusMonth);

        FillReferenceApplicationForm.fillReferenceApplicationForm(isikukood, firstName, lastName, email, phoneNumber, referral, rent, question, formattedMoveInDate, formattedMoveOutDate);
    }

    @Then("^I check if reference is submitted successfully$")
    public void iCheckIfReferenceIsSubmittedSuccessfully() throws Throwable {
        ApplicationView.checkApplication();
    }

    @Then("^I click on menu dropdown and open costumer view$")
    public void iClickOnMenuDropdownClickCostumers() throws Throwable {
        DashboardPage.openCustomers();

    }
    @Then("^I click on contract view$")
    public void iClickOnMenuDropdownAndClickOnContractView() throws Throwable {
        DashboardPage.openContracts();
    }

    @Then("^I click on Create New Client Button and fill Form (\\d+) (.*) (.*) (.*) (\\d+) (.*) (.*) (\\d+) (.*) (\\d+) (\\d+) (\\d+)$")
    public void iClickOnCreateNewClientButtonAndFillForm(Integer isikukood, String firstName, String lastName, String email, Integer phoneNumber, String iban, String streetAdr,Integer houseNr, String city, Integer postalCode, Integer doorNr, Integer floorNr) throws Throwable {
        ClientView.newCustomer(isikukood,firstName,lastName,email, phoneNumber, iban, streetAdr, houseNr, city, postalCode, doorNr, floorNr );
        //throw new PendingException();
    }


//    @Given("^I open (.*)$")
//    public void iOpenCityx(String url) {
//        Configuration.browser = "chrome";
//        open(url);
//    }
    @Then("^I click on Create New Contract Button and fill Form with data (.*) (.*) (.*) (\\d+) (\\d+) (\\d+) (.*)$")
    public void iClickOnCreateNewContractButtonAndFillForm(String contractType,String contractNr, String status,  Integer rent, Integer rentIncrease, Integer collateral,  String customer) throws Throwable {
        //String contractNr, String status,
        //Integer rentIncrease, Integer collateral,
        String createdDate = "2019-02-02";//Calendar.getInstance().toString();
        System.out.printf("test123");
        String signedDate = "";//Calendar.getInstance().toString();
        String terminationDate = "2019-02-02";
        //DateTimeFormatter.format("dd.MM.yyyy")
        Date date = new Date();
        String strDateFormat = "yyyy.MM.dd.HH:mm:ssZ";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate= dateFormat.format(date);

        ContractView.newContract(contractType, contractNr + formattedDate , status, rent ,rentIncrease, collateral, customer);

    }


    @Then("^I create Properties according CSV file (.*)$")
    public void iCreatePropertiesAccordingCSVFile(String csvFilePath)  {

        LaavaPropertyTest.csvTestLaavaProprty(csvFilePath);

    }

    @Then("^I create Contracts according CSV file (.*)$")
    public void iCreateContractsAccordingCSVFileSrcTestResourcesCsvContract_laavaCsv(String csvFilePath) {
        LaavaContractTest.csvTestLaavaContract(csvFilePath);

    }

    @Then("^I create Costumers according CSV file (.*)$")
    public void iCreateCostumersAccordingCSVFileSrcTestResourcesCsvAddress_laavaCsv(String csvFilePath) throws Throwable {
            LaavaCustomerTest.csvTestLaavaCostumer(csvFilePath);
        throw new PendingException();
    }
}

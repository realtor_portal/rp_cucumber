package ee.cityx.cucumber.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.ui.Select;

import static com.codeborne.selenide.Selenide.$;

public class ClientView {

    public static SelenideElement
        createNewClientBtn = $("body > jhi-main > div.container-fluid > div > jhi-customer > div > h2 > button"),
        isikukoodField = $("#field_idCode"),
        firstNameField = $("#field_firstName"),
        lastNameField = $("#field_lastName"),
        emailField = $("#field_email"),
        phoneNumberField = $("#field_phoneNumber"),
        personTypeField = $("#field_typee"),
        languageField = $("#field_language"),
        IBANField = $("#field_iban"),
        streetAdrField = $("#field_street"),
        houseNrField = $("#field_streetNumber"),
        cityField = $("#field_city"),
        postalCodeField = $("#field_postalCode"),
        doorNrField = $("#field_door"),
        floorField = $("#field_floor"),
        objectField = $("#field_rental"),
        saveBtn = $("body > ngb-modal-window > div > div > jhi-customer-dialog > form > div.modal-footer > button.btn.btn-primary");



    public static void newCustomer(Integer isikukood, String firstName, String lastName, String email, Integer phoneNumber, String iban, String streetAdr,Integer houseNr, String city, Integer postalCode, Integer doorNr, Integer floorNr ) {

        createNewClientBtn.click();
        isikukoodField.setValue(isikukood.toString());
        firstNameField.setValue(firstName);
        lastNameField.setValue(lastName);
        emailField.setValue(email);
        phoneNumberField.setValue(phoneNumber.toString());
        //personTypeField.selectOptionByValue("Private");
        //languageField.selectOptionByValue("Estonian");
        IBANField.setValue(iban);
        streetAdrField.setValue(streetAdr);
        houseNrField.setValue(houseNr.toString());
        cityField.setValue(city);
        postalCodeField.setValue(postalCode.toString());
        doorNrField.setValue(doorNr.toString());
        floorField.setValue(floorNr.toString());

        objectField.selectOption(1);
//        $("selector").selectOptionByValue(String... value); // by value from dropdown list
//        $("selector").selectOption(int... index); // by index from dropdown list
//        $("selector").selectOption(String... text); // by text from dropdown list
//        $("selector").selectOptionContainingText(String text); // by containing text from dropdown list

        saveBtn.click();


//        if (!customerSubMenu.is(Condition.visible)) {
//            menuButton.click();
//        }
//        customerSubMenu.click();

    }
    public static void newCustomer(CustomerData customerData ) {

        createNewClientBtn.click();
        isikukoodField.setValue(customerData.getIdCode());
        firstNameField.setValue(customerData.getFirst_name());
        lastNameField.setValue(customerData.getLast_name());
        emailField.setValue(customerData.getEmail());
        phoneNumberField.setValue(customerData.getPhone_number());
        personTypeField.selectOption(customerData.getTypee());
        languageField.selectOption(customerData.getLanguage());
        IBANField.setValue(customerData.getIban());
        streetAdrField.setValue(customerData.getStreet());
        houseNrField.setValue(customerData.getStreet_nr());
        cityField.setValue(customerData.getCity());
        postalCodeField.setValue(customerData.getPostal_code());
        doorNrField.setValue(customerData.getDoor());
        floorField.setValue(customerData.getFloor());
        objectField.selectOption(customerData.getRental());
//        $("selector").selectOptionByValue(String... value); // by value from dropdown list
//        $("selector").selectOption(int... index); // by index from dropdown list
//        $("selector").selectOption(String... text); // by text from dropdown list
//        $("selector").selectOptionContainingText(String text); // by containing text from dropdown list

        saveBtn.click();


//        if (!customerSubMenu.is(Condition.visible)) {
//            menuButton.click();
//        }
//        customerSubMenu.click();

    }


}

package ee.cityx.cucumber.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

//import static com.codeborne.selenide.Selenide.switchTo;
import static com.codeborne.selenide.Selenide.*;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class FillReferenceApplicationForm {

    private static SelenideElement

            viewBtn = $("body > jhi-main > div.container-fluid > div > jhi-property > div > div.table-responsive > table > tbody > tr:nth-child(2) > td:nth-child(7) > div > button.btn.btn-info.btn-sm"),

            viewBtnList = $("body > jhi-main > div.container-fluid > div > jhi-property > div > div.table-responsive > table > tbody > tr:nth-child(4) > td:nth-child(7) > div > button.btn.btn-info.btn-sm > span.fa.fa-eye"),

            fillRefAppLink = $("#application-link"),
            isikukoodField = $("#field_idCode"),
            firstNameField = $("#field_firstName"),
            lastNameField = $("#field_lastName"),
            emailField = $("#field_email"),
            phoneNumberField = $("#field_phoneNumber"),

            referralField = $("#field_referral"),
            rentField = $("#field_rent"),
            questionField = $("#field_question"),
            moveInDateField = $("#field_moveInDate"),
            moveOutDateField = $("#field_moveOutDate"),

            sendBtn = $("body > jhi-main > div.container-fluid > div > jhi-application-form > form > div.modal-footer > button");
///$("#body > jhi-main > div.container-fluid > div > jhi-property > div > div.table-responsive > table > tbody > tr:nth-child(3) > td.text-right > div > button.btn.btn-info.btn-sm > span.d-none.d-md-inline > span")

//#application-link
    public static void fillReferenceApplicationForm(String isikukood, String firstName, String lastName, String email, Integer phoneNumber, String referral, Integer rent, String question, String moveInDate, String moveOutDate) {

        DashboardPage.clickOnProperties();
        if (!FillReferenceApplicationForm.viewBtn.is(Condition.visible)) {
            FillReferenceApplicationForm.viewBtnList.click();

        }else{
            FillReferenceApplicationForm.viewBtn.click();
        }
        FillReferenceApplicationForm.fillRefAppLink.click();
        //Get the current window handle
        switchTo().window(1);
        isikukoodField.setValue(isikukood);
        firstNameField.setValue(firstName);
        lastNameField.setValue(lastName);
        emailField.setValue(email);
        phoneNumberField.setValue(phoneNumber.toString());
        referralField.setValue(referral);
        rentField.setValue(rent.toString());
        questionField.setValue(question);
        moveInDateField.setValue(moveInDate);
        moveOutDateField.setValue(moveOutDate);

        sendBtn.click();
        switchTo().window(0);
    }



}

package ee.cityx.cucumber.page;

import com.opencsv.bean.CsvBindByName;

public class CustomerData {

    @CsvBindByName
    private String idCode;
    @CsvBindByName
    private String first_name;
    @CsvBindByName
    private String last_name;
    @CsvBindByName
    private String email;
    @CsvBindByName
    private String phone_number;
    @CsvBindByName
    private String typee;
    @CsvBindByName
    private String language;
    @CsvBindByName
    private String iban;
    @CsvBindByName
    private String street;
    @CsvBindByName
    private String street_nr;
    @CsvBindByName
    private String city;
    @CsvBindByName
    private String postal_code;
    @CsvBindByName
    private String door;
    @CsvBindByName
    private String floor;
    @CsvBindByName
    private String rental;


    //Getters and setters
    public String getIdCode() {
        return idCode;
    }
    public void setIdCode(String idCode) {
        this.idCode = idCode;
    }

    public String getFirst_name() {
        return first_name;
    }
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {return phone_number; }
    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getTypee() {
        return typee;
    }
    public void setTypee(String typee) {
        this.typee = typee;
    }

    public String getLanguage() {return language; }
    public void setLanguage(String language) {
        this.language = language;
    }

    public String getIban() {
        return iban;
    }
    public void setIban(String iban) { this.iban = iban;}

    public String getStreet() { return street;}
    public void setStreet(String street) { this.street = street;}

    public String getStreet_nr() {
        return street_nr;
    }
    public void setStreet_nr(String street_nr) {
        this.street_nr = street_nr;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getPostal_code() {
        return postal_code;
    }
    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getDoor() {
        return door;
    }
    public void setDoor(String door) {this.door = door;}

    public String getFloor() {
        return floor;
    }
    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getRental() {
        return rental;
    }
    public void setRental(String rental) {
        this.rental = rental;
    }



    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("PropertyData{id=").append(idCode)
            .append(", id=").append(idCode)
            .append(", first_name=").append(first_name)
            .append(", last_name=").append(last_name)
            .append(", email=").append(email)
            .append(", phone_number=").append(phone_number)
            .append(", typee=").append(typee)
            .append(", language=").append(language)
            .append(", iban=").append(iban)
            .append(", street=").append(street)
            .append(", street_nr=").append(street_nr)
            .append(", city=").append(city)
            .append(", postal_code=").append(postal_code)
            .append(", door=").append(door)
            .append(", floor=").append(floor)
            .append(", rental=").append(rental).append("}");

        return builder.toString();
    }



}

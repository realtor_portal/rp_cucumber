package ee.cityx.cucumber.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.switchTo;

public class ApplicationView {

    private static SelenideElement

            viewBtn = $("body > jhi-main > div.container-fluid > div > jhi-application > div > div.table-responsive > table > tbody > tr:nth-child(1) > td.text-right > div > button.btn.btn-info.btn-sm > span.d-none.d-md-inline > span"),
    viewCustomerLink = $("body > jhi-main > div.container-fluid > div > jhi-application > div > div.table-responsive > table > tbody > tr:nth-child(1) > td:nth-child(7) > div > a");



    public static void checkApplication() {

        switchTo().window(0);
        DashboardPage.clickOnApplication();
        ApplicationView.viewBtn.click();
        DashboardPage.clickOnApplication();
        ApplicationView.viewCustomerLink.click();

    }


}

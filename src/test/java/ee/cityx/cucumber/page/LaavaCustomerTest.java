package ee.cityx.cucumber.page;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class LaavaCustomerTest {
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void csvTestLaavaCostumer(String csvFilePath) {
        Path myPath = Paths.get(csvFilePath);

        try (BufferedReader br = Files.newBufferedReader(myPath,
            StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<CustomerData> strategy
                = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(CustomerData.class);

            CsvToBean csvToBean = new CsvToBeanBuilder(br)
                .withSeparator(';')
                .withType(CustomerData.class)
                .withMappingStrategy(strategy)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

            List<CustomerData> customerDataBeans = csvToBean.parse();

            System.out.println("New Contracts will be created with next testdata:");
            customerDataBeans.forEach(x -> System.out.println(x));

            customerDataBeans.forEach(x -> ClientView.newCustomer(x));
            //single test
            //ClientView.newCustomer(customerDataBeans.get(1));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

package ee.cityx.cucumber.page;


//import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
//import au.com.bytecode.opencsv.bean.CsvToBean;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class LaavaContractTest {
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void csvTestLaavaContract(String csvFilePath) {
        Path myPath = Paths.get(csvFilePath);

        try (BufferedReader br = Files.newBufferedReader(myPath,
            StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<ContractData> strategy
                = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(ContractData.class);

            CsvToBean csvToBean = new CsvToBeanBuilder(br)
                .withSeparator(';')
                .withType(ContractData.class)
                .withMappingStrategy(strategy)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

            List<ContractData> contractDataBeans = csvToBean.parse();

            System.out.println("New Contracts will be created with next testdata:");
            contractDataBeans.forEach(x -> System.out.println(x));

            contractDataBeans.forEach(x -> ContractView.newContract(x));
            //single test
            //ContractView.newContract(contractDataBeans.get(1));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

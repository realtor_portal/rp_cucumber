package ee.cityx.cucumber.page;

import com.opencsv.bean.CsvBindByName;

public class PropertyData {


    @CsvBindByName
    private String id;
    @CsvBindByName
    private String province;
    @CsvBindByName
    private String postal_code;
    @CsvBindByName
    private String city;
    @CsvBindByName
    private String street;
    @CsvBindByName
    private String street_number;
    @CsvBindByName
    private String room_count;
    @CsvBindByName
    private String area;
    @CsvBindByName
    private String floor;
    @CsvBindByName
    private String door;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }
    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostal_code() {
        return postal_code;
    }
    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {return street; }
    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet_number() {
        return street_number;
    }
    public void setStreet_number(String street_number) {
        this.street_number = street_number;
    }

    public String getArea() {return area; }
    public void setArea(String area) {
        this.area = area;
    }

    public String getRoom_count() {
        return room_count;
    }
    public void setRoom_count(String room_count) {
        this.room_count = room_count;
    }

    public String getFloor() {
        return floor;
    }
    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getDoor() {
        return door;
    }
    public void setdoor(String door) {
        this.door = door;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("PropertyData{id=").append(id)
            .append(", province=").append(province)
            .append(", city=").append(city)
            .append(", street=").append(street)
            .append(", street_number=").append(street_number)
            .append(", floor=").append(floor)
            .append(", area=").append(area)
            .append(", room_count=").append(room_count)
            .append(", door=").append(door).append("}");

        return builder.toString();
    }

    //Getters and setters


}

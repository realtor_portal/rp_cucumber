package ee.cityx.cucumber.page;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class LoginPage {

    private static SelenideElement
        loginField = $("#username"),
        passwordField = $("#password"),
        signInBtn = $(By.xpath("//div[2]/form/button/span")),
        errorMessage = $$x("//jhi-login-modal/div[2]/div/div[1]/div/span").get(0),
        loginPopupClose = $$x("//jhi-login-modal/div[1]/button/span").get(0);


    public static void signIn(String username, String password) {
        DashboardPage.accountBtn.click();
        DashboardPage.loginBtn.click();
        loginField.setValue(username);
        passwordField.setValue(password);
        signInBtn.click();
    }

    public static void errorMessagesVisible() {
        errorMessage.shouldBe(visible);
    }

    public static void closeLoginPopup() {
        System.out.println("Closing popup");
        loginPopupClose.click();
    }
}

package ee.cityx.cucumber.page;

import com.opencsv.bean.CsvBindByName;

public class ContractData {

    @CsvBindByName
    private String id;
    @CsvBindByName
    private String type;
    @CsvBindByName
    private String contract_number;
    @CsvBindByName
    private String status;
    @CsvBindByName
    private String signed;
    @CsvBindByName
    private String created;
    @CsvBindByName
    private String termination_date;
    @CsvBindByName
    private String rent;
    @CsvBindByName
    private String rent_increase;
    @CsvBindByName
    private String prepayment;
    @CsvBindByName
    private String collateral;
    @CsvBindByName
    private String customer_id;
    @CsvBindByName
    private String realtor_id;
    @CsvBindByName
    private String application_id;


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    public String getContract_number() {
        return contract_number;
    }
    public void setContract_number(String contract_number) {
        this.contract_number = contract_number;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getSigned() {return signed; }
    public void setSigned(String signed) {
        this.signed = signed;
    }

    public String getCreated() {
        return created;
    }
    public void setCreated(String created) {
        this.created = created;
    }

    public String getTermination_date() {return termination_date; }
    public void setTermination_date(String termination_date) {
        this.termination_date = termination_date;
    }

    public String getRent() {
        return rent;
    }
    public void setRent(String rent) {
        this.rent = rent;
    }

    public String getRent_increase() { return rent_increase;}
    public void setRent_increase(String rent_increase) {
        this.rent_increase = rent_increase;
    }

    public String getPrepayment() {
        return prepayment;
    }
    public void setPrepayment(String prepayment) {
        this.prepayment = prepayment;
    }

    public String getCollateral() {
        return collateral;
    }
    public void setCollateral(String collateral) {
        this.collateral = collateral;
    }

    public String getCustomer_id() {
        return customer_id;
    }
    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getRealtor_id() {
        return realtor_id;
    }
    public void setRealtor_id(String realtor_id) {this.realtor_id = realtor_id;}

    public String getApplication_id() {
        return application_id;
    }
    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    @Override
    public String toString() {

        StringBuilder builder = new StringBuilder();
        builder.append("PropertyData{id=").append(id)
            .append(", type=").append(type)
            .append(", contract_number=").append(contract_number)
            .append(", status=").append(status)
            .append(", signed=").append(signed)
            .append(", created=").append(created)
            .append(", termination_date=").append(termination_date)
            .append(", rent=").append(rent)
            .append(", rent_increase=").append(rent_increase)
            .append(", prepayment=").append(prepayment)
            .append(", collateral=").append(collateral)
            .append(", customer_id=").append(customer_id)
            .append(", realtor_id=").append(realtor_id)
            .append(", application_id=").append(application_id).append("}");

        return builder.toString();
    }

    //Getters and setters


}

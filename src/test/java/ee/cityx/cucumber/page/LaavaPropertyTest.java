package ee.cityx.cucumber.page;


import au.com.bytecode.opencsv.CSVReader;
//import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
//import au.com.bytecode.opencsv.bean.CsvToBean;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;


import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class LaavaPropertyTest {
    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void csvTestLaavaProprty(String csvFilePath) {
        Path myPath = Paths.get(csvFilePath);

        try (BufferedReader br = Files.newBufferedReader(myPath,
            StandardCharsets.UTF_8)) {

            HeaderColumnNameMappingStrategy<PropertyData> strategy
                = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(PropertyData.class);

            CsvToBean csvToBean = new CsvToBeanBuilder(br)
                .withSeparator(';')
                .withType(PropertyData.class)
                .withMappingStrategy(strategy)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

            List<PropertyData> propertyDataBeans = csvToBean.parse();

            System.out.println("New properies will be created with next testdata:");
            propertyDataBeans.forEach(x -> System.out.println(x));

            propertyDataBeans.forEach(x -> PropertiesPage.createNewProperty(x));
            //single test
            //PropertiesPage.createNewProperty(propertyDataBeans.get(0));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}

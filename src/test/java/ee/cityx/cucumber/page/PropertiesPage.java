package ee.cityx.cucumber.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class PropertiesPage {

    private static SelenideElement
        nameField = $("#field_name"),
        roomCountField = $("#field_roomCount"),
        areaField = $("#field_area"),
        cityField = $("#field_city"),
        streetField = $("#field_street"),
        streetNrField = $("#field_streetNumber"),
    //body > jhi-main > div.container-fluid > div > jhi-property > div > h2 > button.btn.btn-primary.float-right.jh-create-entity.create-property > span:nth-child(2) > span
    createNewPropertyBtn = $$x("/html/body/jhi-main/div[2]/div/jhi-property/div/h2/button[1]/span[2]/span").get(0),
        saveBtn = $("body > ngb-modal-window > div > div > jhi-property-dialog > form > div.modal-footer > button.btn.btn-primary > span:nth-child(2) > span");


    public static void createNewProperty(String name, String city, String street, String streetNr) {
        DashboardPage.clickOnProperties();
        PropertiesPage.createNewPropertyBtn.click();
        nameField.setValue(name);
        cityField.setValue(city);
        streetField.setValue(street);
        streetNrField.setValue(streetNr);
        saveBtn.click();
    }

    public static void createNewProperty(PropertyData propertyData) {
        DashboardPage.clickOnProperties();
        PropertiesPage.createNewPropertyBtn.click();
        nameField.setValue(propertyData.getId());
        roomCountField.setValue(propertyData.getRoom_count());
        areaField.setValue(propertyData.getArea());
        cityField.setValue(propertyData.getCity());
        streetField.setValue(propertyData.getStreet());
        streetNrField.setValue(propertyData.getStreet_number());
        saveBtn.click();
    }


}

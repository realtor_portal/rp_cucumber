package ee.cityx.cucumber.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ContractView {

    public static SelenideElement
        createNewContractBtn = $("body > jhi-main > div.container-fluid > div > jhi-contract > div > h2 > button > span:nth-child(2) > span"),

        contractTypeField = $("#field_typee"),
        contractNumberField = $("#field_contractNumber"),
        createdField = $("#field_created"),
        statusField = $("#field_status"),
        signedField = $("#field_signed"),
        terminationDateField = $("#field_terminationDate"),
        rentField = $("#field_rent"),
        rentIncreaseField = $("#field_rentIncrease"),
        collateralField = $("#field_collateral"),

        selectDocButton = $("#file_document"),

        applicationField = $("#field_application"),
        customerField = $("#field_customer"),

        saveBtn = $("body > ngb-modal-window > div > div > jhi-contract-dialog > form > div.modal-footer > button.btn.btn-primary > span:nth-child(2) > span");



    public static void newContract(String contractType, String contractNr, String status, Integer rent, Integer rentIncrease, Integer collateral, String customer) {

        createNewContractBtn.click();

        contractTypeField.selectOptionByValue(contractType);
        contractNumberField.setValue(contractNr);
        //createdField.setValue(createdDate);
        statusField.selectOptionByValue(status);
        //signedField.setValue(signed);
        //terminationDateField.setValue(terminationDate);
        rentField.setValue(rent.toString());
        rentIncreaseField.setValue(rentIncrease.toString());
        collateralField.setValue(collateral.toString());
        //selectDocButton.click();
        //applicationField.setValue(application);
        customerField.selectOptionByValue(customer);

        //objectField.selectOption(1);
//        $("selector").selectOptionByValue(String... value); // by value from dropdown list
//        $("selector").selectOption(int... index); // by index from dropdown list
//        $("selector").selectOption(String... text); // by text from dropdown list
//        $("selector").selectOptionContainingText(String text); // by containing text from dropdown list

        saveBtn.click();

    }
    public static void newContract(ContractData contractData) {

        createNewContractBtn.click();

        contractTypeField.selectOptionByValue(contractData.getType());
        contractNumberField.setValue(contractData.getContract_number());
        //createdField.setValue(createdDate);
        statusField.selectOptionByValue(contractData.getStatus());
        signedField.setValue("03/30/2019 01:12 AM");
        terminationDateField.setValue(contractData.getTermination_date());
        rentField.setValue(contractData.getRent());
        rentIncreaseField.setValue(contractData.getRent_increase());
        collateralField.setValue(contractData.getCollateral());
        //selectDocButton.click();
        if(contractData.getApplication_id() != "null"){
            applicationField.selectOption(contractData.getApplication_id());
        }

        customerField.selectOption(contractData.getCustomer_id());

        saveBtn.click();

    }


}

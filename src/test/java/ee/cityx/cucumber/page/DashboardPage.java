package ee.cityx.cucumber.page;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$x;

public class DashboardPage {

    public static SelenideElement
        accountBtn = $("#account-menu"),
        loginBtn = $("#login"),
        logoutBtn = $("#logout"),
        openMenuTab = $("#entity-menu > span"),
        applicationBtn = $("#navbarResponsive > ul > li:nth-child(4) > a > span > span > span"),
        propertyButton = $("#navbarResponsive > ul > li:nth-child(2) > a > span > span > span"),
        menuButton = $("#entity-menu > span > span > span"),
        customerSubMenu = $("#navbarResponsive > ul > li.nav-item.pointer.show.dropdown > ul > li:nth-child(4) > a > span > span"),
        contractMenu = $("#navbarResponsive > ul > li:nth-child(3) > a > span > span > span");



    private static SelenideElement dropdown = $$x("//*[@id=\"navbarResponsive\"]/ul/li[6]/ul").get(0);


    public static void logoutExists() {
        if (!dropdown.is(Condition.visible)) {
            accountBtn.click();
        }
        logoutBtn.exists();
    }

    public static void logout() {
        if (!dropdown.is(Condition.visible)) {
            accountBtn.click();
        }
        logoutBtn.click();
    }

    public static void clickOnProperties() {
        propertyButton.click();
    }

    public static void clickOnApplication() {
        openMenuTab.click();
        applicationBtn.click();
    }
    public static void openCustomers() {

        menuButton.click();

        if (!customerSubMenu.is(Condition.visible)) {
            menuButton.click();
        }
        customerSubMenu.click();

    }
    public static void openContracts() {

        contractMenu.click();

    }
}
